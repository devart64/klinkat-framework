<?php
/**
 * Created by PhpStorm.
 * User: steph
 * Date: 29/01/2018
 * Time: 13:43
 */

namespace Framework;

class Module
{
    const DEFINITIONS = null;

    const MIGRATIONS = null;

    const SEEDS = null;
}
