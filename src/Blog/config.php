<?php
/**
 * Created by PhpStorm.
 * User: steph
 * Date: 29/01/2018
 * Time: 13:37
 */

use App\Blog\BlogModule;
use function \DI\object;
use function \DI\get;

return [
    'blog.prefix' => '/blog',
    BlogModule::class => object()->constructorParameter('prefix', get('blog.prefix'))
];
